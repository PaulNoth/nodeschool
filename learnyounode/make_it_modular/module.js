'use strict';

var fs = require('fs');

var filterFiles = function filterFiles(dir, ext, callback) {
    fs.readdir(dir, (err, list) => {
        if(err) {
            return callback(err);
        }
        let filtered = list.filter(file => file.substring(file.lastIndexOf('.')) === '.' + ext);
        return callback(null, filtered);
    });
};

module.exports = filterFiles;
