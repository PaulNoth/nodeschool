/*
 Write a program that performs an HTTP GET request to a URL provided to you
 as the first command-line argument. Collect all data from the server (not
 just the first "data" event) and then write two lines to the console
 (stdout).

 The first line you write should just be an integer representing the number
 of characters received from the server. The second line should contain the
 complete String of characters sent by the server.
 */

'use strict';

var http = require('http');
var bl = require('bl');

let url = process.argv[2];

http.get(url, response => {
   response.pipe(bl((err, data) => {
       if(err) {
           console.error(err);
       }
       let text = data.toString();
       console.log(text.length);
       console.log(text);
   }));
});