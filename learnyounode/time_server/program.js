/*
 Write a TCP time server!

 Your server should listen to TCP connections on the port provided by the
 first argument to your program. For each connection you must write the
 current date & 24 hour time in the format:

 "YYYY-MM-DD hh:mm"

 followed by a newline character. Month, day, hour and minute must be
 zero-filled to 2 integers. For example:

 "2013-07-06 17:42"
 */

'use strict';

var net = require('net');

let port = process.argv[2];

let prefixed = (data, max) => {
    let prefix = max - data.toString().length;
    let result = '';
    for(let i = 0; i < prefix; i++) {
        result += '0';
    }
    result += data.toString();
    return result;
};

let server = net.createServer(socket => {
    let date = new Date();
    let year = prefixed(date.getFullYear(), 4);
    let month = prefixed(date.getMonth() + 1, 2);
    let day = prefixed(date.getDate(), 2);
    let hours = prefixed(date.getHours(), 2);
    let minutes = prefixed(date.getMinutes(), 2);

    let data = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes;
    socket.write(data);
    socket.end();
});
server.listen(port);