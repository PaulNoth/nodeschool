/*
 Write a program that accepts one or more numbers as command-line arguments
 and prints the sum of those numbers to the console (stdout).
 */

'use strict';

let args = process.argv.slice(2).map(i => parseInt(i));
let sum = 0;
for(let i = 0; i < args.length; i++) {
    sum += args[i];
}

console.log(sum);