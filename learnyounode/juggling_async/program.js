/*
 This problem is the same as the previous problem (HTTP COLLECT) in that
 you need to use http.get(). However, this time you will be provided with
 three URLs as the first three command-line arguments.

 You must collect the complete content provided to you by each of the URLs
 and print it to the console (stdout). You don't need to print out the
 length, just the data as a String; one line per URL. The catch is that you
 must print them out in the same order as the URLs are provided to you as
 command-line arguments.
 */

'use strict';

var http = require('http');
var bl = require('bl');

var urls = [process.argv[2], process.argv[3], process.argv[4]];

var results = [];
var count = 0;

for(let i = 0; i < urls.length; i++) {
    http.get(urls[i], response => {
       response.pipe(bl((err, data) => {
           if(err) {
               console.log(err);
           }
           results[i] = data.toString();
           count++;

           if(count === 3) {
               console.log(results.join('\n'));
           }
       }));
    });
}