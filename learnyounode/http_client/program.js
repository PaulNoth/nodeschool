/*
 Write a program that performs an HTTP GET request to a URL provided to you
 as the first command-line argument. Write the String contents of each
 "data" event from the response to a new line on the console (stdout). */
'use strict';

var http = require('http');

let url = process.argv[2];

http.get(url, response => {
   response.on("data", data => {
      console.log(data.toString());
   });
});

/*
 Here's the official solution in case you want to compare notes:

 ───────────────────────────────────────────────────────────────────────

 var http = require('http')

 http.get(process.argv[2], function (response) {
 response.setEncoding('utf8')
 response.on('data', console.log)
 response.on('error', console.error)
 })
 */